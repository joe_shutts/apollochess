#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <limits.h>

#define WHITE 0
#define BLACK 1
#define MAX_PIECES 32
#define BOARD_SIZE 8   /*array size*/

int state;
int computer_colour;
int stop_input;

enum pieces{
  NONE = 0,
  PAWN = 1,
  KNIGHT = 2,
  BISHOP = 3,
  ROOK = 4,
  QUEEN = 5,
  KING = 6,
  BLACK_PAWN,
  BLACK_KNIGHT,
  BLACK_BISHOP,
  BLACK_ROOK,
  BLACK_QUEEN,
  BLACK_KING
};

struct move{
  int piece[2];           /* the piece to be moved */
  int position[2];          /* the position to move the piece to */
  struct move *next;
  struct branchHead *nextMoves;
  int upgrade;
  //int castle;            /* 1 is Queenside 2 is Kingside numbers < 0 are for black*/
  //int enPassant;        /*file of en passant move. -1 means no move*/
};

struct board{
  int squares[BOARD_SIZE][BOARD_SIZE];  
  int canCastle[2];  ;//1 can caste Queenside, 2 kingside, 3 both, 0 none. index 0, is white, 1 is black
  int enPassant;  /*file of possible en passant move */
};

void printBoard(struct board *board);

struct branchHead{
  struct board *board;
  struct move *list;
};

/* ui related */

#define BUF_LENGTH 32
#define EXIT_MESSAGE "\n\n   Thank you for using Apollo Chess \n\n"
enum{
  IDLE,
  USER_MOVE,
  EXIT,
  NEW,
  COMPUTER_MOVE,
  SWITCH_COLOUR,
  MOVE_NOW
};

struct move *scan(struct board *board, int colour);
struct move *select_move(struct board *gameboard);
void initialise_board(struct board *board);
void get_input();
struct board *updateBoard(struct board *old_board, struct move *move, int colour);
