/*    This file is part of ApolloChess.

    ApolloChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Freeoftware Foundation, either version 3 of the License, or
    (at your option) any later version.

    ApolloChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ApolloChess.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Apollo.h"
#include <sys/time.h>

struct board *updateBoard(struct board *old_board, struct move *move, int colour){
  struct board *board;
  board = malloc(sizeof(struct board));
  memcpy(board, old_board, sizeof(struct board));
  if(!move){
    printf("WARNING: Null move\n");
    return NULL;
  }
  if((board->squares[move->piece[0]][move->piece[1]]==KING+colour*KING)&&(move->piece[1]==move->position[1])){   /*handle castling*/
    board->canCastle[colour]=0;
    if(move->position[0]-move->piece[0]==2){
      board->squares[move->piece[0]][move->piece[1]]=0;
      board->squares[move->position[0]][move->position[1]]=KING+colour*KING;
      board->squares[7][colour*7]=0;
      board->squares[5][0]=ROOK+colour*KING;
      return board;
    }
    else{
      board->squares[move->piece[0]][move->piece[1]]=0;
      board->squares[move->position[0]][move->position[1]]=KING+colour*KING;
      board->squares[0][colour*7]=0;
      board->squares[7][3]=ROOK+colour*KING;
      return board;
    }
  }
  if((board->squares[move->piece[0]][move->piece[1]]==PAWN+6*colour)&&(move->position[1]-move->piece[1]==(2-4*colour)))
    board->enPassant=move->piece[0];
  else
    board->enPassant=-1;
  board->squares[move->position[0]][move->position[1]] = board->squares[move->piece[0]][move->piece[1]];
  board->squares[move->piece[0]][move->piece[1]] = 0;
  if(board->squares[move->position[0]][move->position[1]]==PAWN+colour*KING&&old_board->squares[move->position[0]][move->position[1]]==0&&abs(move->position[0]-move->piece[0])==1){
    board->squares[move->position[0]][move->position[1]-1+(2*colour)]=0;
  }
  if(move->upgrade>0){
    board->squares[move->position[0]][move->position[1]]=1+move->upgrade+colour*KING;
  }
  return board;
}

int checkCheck(struct board board, int colour){ /*checks if colour is in check doesnt work*/
  int king[2];
  int pieces[MAX_PIECES/2][2];
  int c =0, dy, dx, i,j,k, iter, iter_y;
  for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      if(board.squares[i][j]==KING+(colour*KING)){
	king[0]=i;
	king[1]=j;
      }
      else if(board.squares[i][j]!=0&&board.squares[i][j]>6!=colour&&board.squares[i][j]>0){
	pieces[c][0] = i;
	pieces[c][1] = j;
	c++;
      }
    }
  }
  for(i=0;i<c;i++){
    printf("\n%i, %i\n", i, board.squares[pieces[i][0]][pieces[i][1]]);
    dx = king[0]-pieces[i][0];
    dy = king[1]-pieces[i][1];
switch((board.squares[pieces[i][0]][pieces[i][1]])-(!colour*KING)){
    case KING:
      if(abs(king[0]-pieces[i][0])&&abs(king[1]-pieces[i][1]))
	return 1;
      else
	break;
    case QUEEN:
      if(dy==0){
	iter = 1-2*(dx<0);
	for(j=1;j<dx;j=j+iter){
	  if(board.squares[pieces[i][0]+j][pieces[i][1]+j]!=0)
	goto BLOCKED;
      }
      }
      else if(dx==0){
    iter = 1-2*(dx&(1<<31));
    for(j=1;j<dy;j=j+iter){
      if(board.squares[pieces[i][0]][pieces[i][1]+j]!=0)
	goto BLOCKED;
    }
      }else if(abs(dx)==abs(dy)){
	if(dx&(1<<31)){
	  if(dy&(1<<31)){
		for(j=0;j<-dx-1;i++){
		  if(board.squares[pieces[i][0]-j][pieces[i][1]-j]!=0)
		    goto BLOCKED;
		}
	  }
	  else{
	    for(j=0;j<dy-1;j++){
	      if(board.squares[pieces[i][0]-j][pieces[i][1]+j]!=0)
		goto BLOCKED;
	    }
	  }
     }
	else if(dy&(1<<31)){
	 for(j=0;j<dx-1;i++){
	   if(board.squares[pieces[i][0]-j][pieces[i][1]+j]!=0)
	     goto BLOCKED;
	 }
       }
       else{
	 for(j=0;j<dy-1;j++){
	   if(board.squares[pieces[i][0]+j][pieces[i][1]+j]!=0)
	   goto BLOCKED;
       }
       }
     }
   return 1;
 BLOCKED:
   break;
 case PAWN:
   if(pieces[i][1]+1-(~colour*2)==king[1]){
     if(pieces[i][0]+1==king[0]|| pieces[c][0]-1==king[0])
       return 1;
   }
   break;
 case KNIGHT:
   if(abs(dy)==2){
       if(abs(dx)==1)
	 return 1;
     }
     else if(abs(dx)==2){
       if(abs(dy)==1)
	 return 1;
     }
     
     break;
   case BISHOP:
     if(abs(dx)!=abs(dy))
	break;
     else{
       if(dx>0)
	 iter = 1;
       else
	 iter = -1;
       if(dy>0)
	 iter_y =1;
       else
	 iter_y=-1;
       for(j=0;j<dx-1;j++){
	 if(board.squares[pieces[i][0]+iter*j][pieces[i][1]+iter_y*j]==0)
	   goto BLOCKED;
       } 
     }
     return 1;
       break;
     case ROOK:
	if(dx==0){
	  if(dy>0){
	    for(j=0;j<dy-1;j++){
	      if(board.squares[pieces[i][0]][pieces[i][1]+j]!=0)
		goto BLOCKED;
	    }
	    return 1;
	  }
	  else if(dy<0){
	    for(j=0;j<-dy-1;j++){
	      if(board.squares[pieces[i][0]][pieces[i][1]-j]!=0)
		goto BLOCKED;
		  }
	    return 1;
	  }
	}
	else if(dy==0){
	  if(dx>0){
	    for(j=0;j<dy-1;j++){
	      if(board.squares[pieces[i][0]+j][pieces[i][1]]!=0)
		goto BLOCKED;
	    }
	    return 1;
	  }
	  else if(dy<0){
	    for(j=0;j<-dy-1;j++){
	      if(board.squares[pieces[i][0]-j][pieces[i][1]]!=0)
		goto BLOCKED;
	    }
	    return 1;
	  }
	  else
	    break;
	}
 }
  
  }
  return 0;
}
struct move *add_move(struct board *board, struct move *current_move, int piece[], int x, int y,int colour){
  if(x>=BOARD_SIZE || x < 0 || y < 0 || y >= BOARD_SIZE)
    return current_move;
  if(board->squares[x][y]>6!=colour||board->squares[x][y]==0){
    current_move->next = malloc(sizeof(struct move));
    current_move=current_move->next;
    current_move->piece[0] = piece[0];
    current_move->piece[1] = piece[1];
    current_move->position[0] = x;
    current_move->position[1] = y;
    current_move->upgrade=0;
  }
  return current_move;
}
struct move *scan_king(struct board *board, int position[1], int colour){
  int x, y ,i;
  struct move *head, *current_move;
  current_move = malloc(sizeof(struct move));
  head = current_move;
  for(i=0;i<8;i++){
    switch(i){
    case 0:
      x=-1;
      y=1;
      break;
    case 1:
      x=0;
      y=1;
      break;
    case 2:
      x=1;
      y=1;
      break;
    case 3:
      x=-1;
      y=0;
      break;
    case 4:
      x=1;
      y=0;
      break;
    case 5:
      x= -1;
      y=-1;
      break;
    case 6:
      x=0;
      y=-1;
      break;
    case 7:
      x=1;
      y=-1;
      break;
    }
    current_move = add_move(board, current_move, position, position[0]+x, position[1]+y, colour);
  }
  if((board->canCastle[colour]&1)==1){ //queenside
     int canCastle = 1;
     //check no pieces in between
     for(i=1;i<3;i++){
       if(board->squares[i][colour*7]!=0){
	 canCastle=0;
	 break;
       }
     }
     
      if(canCastle==1){
	current_move->next = malloc(sizeof(struct move));
	current_move = current_move->next;
	//current_move->castle=1-2*colour;
      }
     
  }
  if((board->canCastle[colour]&2)!=0&&board->squares[5][colour*7]==0&&board->squares[6][colour*7]==0)
     {
       current_move->next=malloc(sizeof(struct move));
       current_move=current_move->next;
       //current_move->castle=2-4*colour;
  }
   
  return head->next;
}
struct move *scan_knight(struct board *board, int position[], int colour){
  int x, y ,i;
  struct move *head, *current_move;
  current_move = malloc(sizeof(struct move));
  head = current_move;
  for(i=0;i<8;i++){
    switch(i){
    case 0:
      x=1;
      y=2;
      break;
    case 1:
      x=2;
      y=1;
      break;
    case 2:
      x=-1;
      y=2;
      break;
    case 3:
      x=-2;
      y=1;
      break;
    case 4:
      x=1;
      y=-2;
      break;
    case 5:
      x= 2;
      y=-1;
      break;
    case 6:
      x=-1;
      y=-2;
      break;
    case 7:
      x=-2;
      y=-1;
      break;
    }
    current_move = add_move(board, current_move, position, position[0]+x, position[1]+y, colour);
  }
  return head->next;
}     
struct move *scan_pawn(struct board *board, int position[], int colour){
  struct move *current_move, *head;
  int start_take=0, i;
  int move_exists =0;
  current_move=malloc(sizeof(struct move));
  current_move->upgrade=0;
  head=current_move;
  if(position[1]+1-2*colour>7)
    return NULL;
  if(position[1] == (5*colour)+1&&board->squares[position[0]][position[1]+2*(1-2*colour)]==0&&board->squares[position[0]][position[1]+(1-2*colour)]==0){  //at starting position
      current_move->piece[0]=position[0];
      current_move->piece[1]=position[1];
      current_move->position[0] = position[0];
      current_move->position[1] =position[1]+2-4*colour;
      current_move->next=malloc(sizeof(struct move));
      current_move=current_move->next;
      move_exists=1;
  }
  if(position[0]+1<7){
    if((board->squares[position[0]+1][position[1]+1-2*colour]>6)==!colour && (board->squares[position[0]+1][position[1]+1-2*colour]!=0)){
      current_move->piece[0]=position[0];
      current_move->piece[1]=position[1];
      current_move->position[0] = position[0]+1;
      current_move->position[1] =position[1]+1-2*colour;
      current_move->next=malloc(sizeof(struct move));
    current_move=current_move->next;
    move_exists=1;
    }
  }
  if(position[0]-1>=0){
    if((board->squares[position[0]-1][position[1]+1-2*colour]>6)==!colour && (board->squares[position[0]-1][position[1]+1-2*colour]!=0)){
      current_move->piece[0]=position[0];
      current_move->piece[1]=position[1];
      current_move->position[0] = position[0]-1;
      current_move->position[1] =position[1]+1-2*colour;
      current_move->next=malloc(sizeof(struct move));
      current_move=current_move->next;
      move_exists=1;
    }
  }
  if(board->squares[position[0]][position[1]+1-2*colour]==0){
    move_exists=1;
    if(position[1]==(1+5*!colour)){ //if can promote
      for(i=2;i<=5;i++){
	current_move->piece[0]=position[0];
	current_move->piece[1]=position[1];
	current_move->position[0]=position[0];
	current_move->position[1]= position[1]+1-2*colour;
	current_move->upgrade = i;
	current_move->next=malloc(sizeof(struct move));
	current_move=current_move->next;	  
      }
      current_move->piece[0]=position[0];
      current_move->piece[1]=position[1];
      current_move->position[0]=position[0];
      current_move->position[1]=!colour*7;
    }
    else{
      current_move->piece[0]=position[0];
      current_move->piece[1]=position[1];
      current_move->position[0]= position[0];
      current_move->position[1]=position[1]+1+colour*-2;
    }
  }
  if(board->enPassant>=0&&(abs(position[0]-board->enPassant)==1)&&(position[1]==4+!colour)){
    move_exists =1;
    current_move->next = malloc(sizeof(struct move));
    current_move = current_move->next;
  }
  if(move_exists==0)
    return NULL;
  return head;
}
struct move *scan_bishop(struct board *board ,int position[], int colour){
  int i, j;
  int take, x, y;
  struct move *current_move;
  struct move *head;
  current_move = malloc(sizeof(struct move));
  head = current_move;
    for(i=0;i<4;i++){
      x=0;  
      y=0;
      take=-1;
      while(position[0]+x < BOARD_SIZE && position[1]+y <BOARD_SIZE && position[0]+x>0&&position[1]+y>0){
	if(x!=0||y!=0){
	  if(board->squares[position[0]+x][position[1]+y]!=0){
	    take=board->squares[position[0]+x][position[1]+y]>6;
	    break;
	  }
	  
	  else{
	    current_move->next = malloc(sizeof(struct move));
	    current_move=current_move->next;
	    current_move->piece[0] = position[0];
	    current_move->piece[1] = position[1];
	    current_move->position[0] = position[0]+x;
	    current_move->position[1] = position[1]+y;
	  }
	}
	switch(i){
	case 0:
	  x++;
	  y++;
	  break;
       case 1:
	 x++;
	 y--;	 
	 break;
       case 2:
	 x--;
	 y--;
	 break;
       case 3:
	 x--;
	 y++;
	 break;
       }
      }
      if(take!=colour&&take>0){
      	  current_move->next = malloc(sizeof(struct move));
	  current_move=current_move->next;
	  current_move->piece[0] = position[0];
	  current_move->piece[1] = position[1];
	  current_move->position[0] = position[0]+x;
	  current_move->position[1] = position[1]+y;
    }
    }
    return head->next;
}

struct move *scan_rook(struct board *board ,int position[], int colour){
  int i, j;
  int take, x, y;
  struct move *current_move;
  struct move *head;
  current_move = malloc(sizeof(struct move));
  head = current_move;
  for(i=0;i<4;i++){
    x=0;  
    y=0;   
    while(position[0]+x < BOARD_SIZE && position[1]+y <BOARD_SIZE && position[0]+x>=0&&position[1]+y>=0){
      take=-1;
	if(x!=0||y!=0){
	  if(board->squares[position[0]+x][position[1]+y]!=0){
	  take=board->squares[position[0]+x][position[1]+y]>6!=colour;
	  break;
	  }
	  
	  else{
	    current_move->next = malloc(sizeof(struct move));
	    current_move=current_move->next;
	    current_move->piece[0] = position[0];
	    current_move->piece[1] = position[1];
	    current_move->position[0] = position[0]+x;
	    current_move->position[1] = position[1]+y;
	  }
	}
	switch(i){
	case 0:
	  x++;
	  break;
	case 1:
	  y++;
	  break;
	case 2:
	  x--;
	  break;
	case 3:
	  y--;
	  break;
	}
	
    }
    if(take==1){
      current_move->next = malloc(sizeof(struct move));
      current_move=current_move->next;
      current_move->piece[0] = position[0];
      current_move->piece[1] = position[1];
      current_move->position[0] = position[0]+x;
      current_move->position[1] = position[1]+y;
    }
    
    
  }
  return head->next;
}
struct move *scan_queen(struct board *board ,int position[], int colour){
  struct move *current_move;
  struct move *head;
  head = scan_bishop(board, position, colour);
  if(!head)
    return NULL;
  current_move=head;
  while(current_move)
    current_move=current_move->next;
  current_move = scan_rook(board, position, colour);
  return head;
}

struct move *scan(struct board *board, int colour){
  struct move *current_move, *head;
  int i,j;
  int position[2];
  current_move = malloc(sizeof(struct move));
  current_move->next=NULL;
  head=current_move;
  for(i=0;i<8;i++){
    position[0]=i;
    for(j=0;j<8;j++){
      //printf("%i, %i\n", i, j);
      position[1]=j;   
      switch((board->squares[i][j])-(colour*KING)){
      case PAWN:
	current_move->next = scan_pawn(board, position, colour);
	break;
      case BISHOP:
	current_move->next = scan_bishop(board, position, colour);
	break;
      case KNIGHT:
	current_move->next = scan_knight(board, position, colour);
	break;
      case ROOK:
	current_move->next = scan_rook(board, position, colour);
	break;
      case QUEEN:
	current_move->next = scan_queen(board, position, colour);
	break;
      case KING:
	current_move->next = scan_king(board, position, colour);
	break;
      default:
	break;
      }
       while(current_move->next){
	 current_move=current_move->next;
       }
    }	
  }
  return head->next;
}
void printBoard(struct board *board){
  int i,j;
  for(i=0;i<8;i++){
    printf("\n");
    for(j=0;j<8;j++){
      printf("%i ", board->squares[i][j]);
    }
  }
  printf("\n");
}
void freeBranches(struct branchHead *branchHead){
  struct move *current_move;
  struct move *tmp_move;
  if(!branchHead)
    return;
  current_move=branchHead->list;
  while(current_move){
    freeBranches(current_move->nextMoves);
    tmp_move=current_move->next;
    free(current_move);
    current_move=tmp_move;
    }
  free(branchHead->board);
  free(branchHead);
  branchHead=NULL;
  return;
}
int score_board(struct board *board){
  int i,j;
  int score=0;
  int colour;
  int piece;
  for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      colour=board->squares[i][j]>6;
      piece=board->squares[i][j]-colour*6;
      if(piece!=KING)
	score=score+piece*(1-2*colour);
    }
  }
  return score;
}
int mini_max(struct branchHead *branchHead, int depth, int alpha, int beta, int colour){ //colour to move returns index of best move
  struct move *current_move;
  struct move *tmp_move;
  int best_score;
  int tmp_score;
  get_input();
  if(depth<=0){
    return score_board(branchHead->board);
  }
  if(state!=COMPUTER_MOVE){
    return 0;
  }
  if(!branchHead)
    return 0;
  branchHead->list=scan(branchHead->board, colour);
  if(!branchHead->list)
    return 0;
  current_move=branchHead->list;
  if(colour){
    best_score=-INT_MAX;
    tmp_score=best_score;
    while(current_move){
      current_move->nextMoves = malloc(sizeof(struct branchHead));
      current_move->nextMoves->board=updateBoard(branchHead->board, current_move, colour);
      tmp_score = mini_max(current_move->nextMoves, depth-1, alpha, beta, !colour);
      if(tmp_score>best_score)
	best_score=tmp_score;
      if(best_score>alpha)
      alpha=best_score;
      if(beta<=alpha)
	break;
      current_move=current_move->next;
    }
  }
  else{
    best_score=INT_MAX;
    tmp_score=best_score;
    while(current_move){
      current_move->nextMoves=malloc(sizeof(struct branchHead));
      current_move->nextMoves->board=updateBoard(branchHead->board, current_move, colour);
      tmp_score = mini_max(current_move->nextMoves, depth-1, alpha, beta, !colour);
      if(tmp_score<best_score)
	best_score=tmp_score;
      if(best_score<alpha)
	alpha=best_score;
      if(beta<=alpha)
	break;
      if(!current_move->next)
	break;
      if(!current_move->next)
	break;
      current_move=current_move->next;
      }
  }
  return best_score;
}
void free_tree(struct move *move);
void purge_branches(struct branchHead *branchHead){
  if(!branchHead)
    return;
  if(branchHead->board){
    free(branchHead->board);
  }
  free_tree(branchHead->list);
  free(branchHead);
  branchHead=NULL;
}

void free_tree(struct move *move){
  struct move *current_move;
  struct move *tmp_move;
  if(!move)
    return;
  current_move=move;
  while(current_move){
    if(current_move->nextMoves)
      purge_branches(current_move->nextMoves);
    if(!current_move->next)
      return;
    tmp_move=current_move->next;
    free(current_move);
    current_move=tmp_move;
  }
}
