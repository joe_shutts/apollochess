#include "Apollo.h"
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
char user_move[BUF_LENGTH];

struct command *command_head;
struct command{
  char command[BUF_LENGTH];
  int is_set;
  struct command *next;
};
int stop_input;

void ponder(struct branchHead *branchHead){
  return;
}

int letter_or_number(char a){                          /* returns 0 for neither, 1 for letter and 2 for int */
  if(a>='a'&& a<='z')
    return 1;
  else if(a>='0'&& a<='9')
    return 2;
  else
    return 0;
}

void *store_input(){
  struct command *current_command;
  char buffer[BUF_LENGTH];
  command_head=malloc(sizeof(struct command));
  current_command=command_head;
  current_command->is_set=0;
  while(1){
    fgets(current_command->command, BUF_LENGTH, stdin);
    current_command->is_set=1;
    printf(">%s", current_command->command);
    current_command->next = malloc(sizeof(struct command));
    current_command=current_command->next;
    current_command->is_set=0;
    if(stop_input==1){
      pthread_exit(NULL);
      return NULL;
    }
  } 
}

struct board *newBoard(){
  struct board *ret;
  ret = malloc(sizeof(struct board));
  ret->enPassant = -1;
  ret->canCastle[0] =  0;
  ret->canCastle[1]=0;
  return ret;
}

int main(){
  pthread_t input_thread;
  struct board gameboard;
  struct board *tmp_board;
  int tmp_piece;
  int can_ponder;
  struct move *move, *tmp_move=NULL;;
  struct branchHead branchHead;
  stop_input = 0;
  branchHead.board=NULL;
  pthread_create(&input_thread, NULL, store_input, NULL);  
  setbuf(stdout, NULL);
  signal(SIGINT,SIG_IGN);
  state = NEW;
  can_ponder=0;
  printf("\n Welcome to Apollo Chess\n    User is playing as white, Computer is black.\n    Enter move in the form a1g8 \n");
  
  while(1){
    if(state==EXIT){
      stop_input=1;
      fputs("hi\n",stdin);
      pthread_join(input_thread, NULL);
      return 0;
      break;
    }
    if(state==USER_MOVE){
      if(strlen(user_move)>0){
	move=malloc(sizeof(struct move));
	move->upgrade=0;
      	if(strlen(user_move)==5){
	  switch(user_move[5]){
	  case 'k':
	    move->upgrade=KNIGHT;
	    break;
	  case 'b':
	    move->upgrade=BISHOP;
	    break;
	  case 'r':
	    move->upgrade=ROOK;
	    break;
	  case 'q':
	    move->upgrade=QUEEN;
	    break;
	  case '\0':
	  case '\r':
	    break;
	  default:
	    printf("Error (ambiguous move)");
	    break;
	  }
	}
	move->piece[0]=user_move[0]-'a';
	move->piece[1]=user_move[1]-'1';
	move->position[0]=user_move[2]-'a';
	move->position[1]=user_move[3]-'1';
	if(check_move(&gameboard, move) != 0){
	  printf("Illegal move \n");
	  state=USER_MOVE;
	}
	else{
       	  tmp_board = updateBoard(&gameboard, move, !computer_colour);
	  if(tmp_board)
	    memcpy(&gameboard, tmp_board, sizeof(struct board));
	  else{
	    printf("Error, stopping program \n");
	    return 0;
	  }
	  state=COMPUTER_MOVE;
	}
	free(move);
	memset(user_move, 0, BUF_LENGTH);
	user_move[0] = '\0';
	printBoard(&gameboard);
      }
    }
    if(state==COMPUTER_MOVE){
      tmp_move=select_move(&gameboard);
      if(!tmp_move){
	printf("No valid moves! Restarting. \n");
	state=NEW;
	continue;
	}
      else{
	memcpy(&gameboard, updateBoard(&gameboard, tmp_move , computer_colour),sizeof(struct board));
	printBoard(&gameboard);
	state=USER_MOVE;
      }
    }
    if(state==SWITCH_COLOUR){
      state=IDLE;
    }
    if(state==NEW){
      if(branchHead.board){
	free_tree(branchHead.list);
	free(branchHead.board);
      }
      branchHead.board = malloc(sizeof(struct board));
      computer_colour=BLACK;
      can_ponder=0;
      initialise_board(&gameboard);
      memcpy(branchHead.board, &gameboard, sizeof(struct board));
      state=USER_MOVE;
    }
    if(state==IDLE){
      if(can_ponder==1)
	ponder(&branchHead);
    }
    get_input();
  }
}

void get_input(){                      /*takes the first command and changes the variable, state */
  struct command *tmp_command;
  int is_move = 0;
  if(state==EXIT){
    return;
    }
  if(!command_head)
    return;
  if(!command_head->command)
    return;
  if(command_head->is_set==0)
    return;
  else{
      if(strlen(command_head->command)>0){
	int count=0;
	if(strlen(command_head->command)==4||strlen(command_head->command)==5){
	  is_move=1;
	  for(count=0;count<strlen(command_head->command)-1;count++){
	    if(letter_or_number(command_head->command[count])%2!=(count+1)%2){
	      is_move=0;
     	      break;
	    }
	  }
	  if(is_move==1){
	    state=USER_MOVE;
	    strcpy(user_move, command_head->command);
	  }
	}                   // loop through commands
	if(strncmp("?", command_head->command,1)==0){
	  state=MOVE_NOW;
	  return;
	}
	else if(strncmp("quit", command_head->command,4)==0){
	  printf(EXIT_MESSAGE);
	  state=EXIT;
	}
	else if(strncmp("white", command_head->command,5)==0){
	  if(computer_colour==BLACK){
	    state=SWITCH_COLOUR;
	    computer_colour=!computer_colour;
	  }
	}
	else if(strncmp("black", command_head->command,5)==0){
	  if(computer_colour==WHITE){
	    computer_colour=!computer_colour;
	    state=SWITCH_COLOUR;
	  }
	}
	else if(strncmp("new", command_head->command,3)==0)
	  state=NEW;
      }
      printf("\n%s\n", command_head->command);
      if(command_head->next){
	tmp_command=command_head;
	command_head=command_head->next;
	free(tmp_command);
      }
      else{
	command_head->next = malloc(sizeof(struct command));
	tmp_command=command_head->next;
	free(command_head);
	command_head=tmp_command;
      }
  }
}
int check_move(struct board *board, struct move *move){
  struct move *move_head;
  struct move *current_move;
  int move_hash;
  int check_move=1;
  move_head = scan(board, !computer_colour);
  current_move = move_head;
  while(current_move){
    if(cmp_move(current_move, move)==1)
      check_move=0;
    current_move=current_move->next;
  }
  free_tree(current_move);
  return check_move;
}

int cmp_move(struct move *a, struct move *b){
  if(!a || !b)
    return 0;
  if(a->piece[0]==b->piece[0]&&b->piece[1]==a->piece[1]&&a->position[0]==b->position[0]&& a->position[1]==b->position[1]&& a->upgrade==b->upgrade)
    return 1;
  else
    return 0;
}
struct move *select_move(struct board *gameboard){
   int tmp_score;
   int count=0;
   int best_score=0;
   int move_index=0;
   struct move *current_move;
   struct move *tmp_move;
   struct move *move_head;
   int new_tree=1;
   /*if(branchHead->list){
     current_move = branchHead->list;
     while(current_move){
       if(!current_move->nextMoves){
	 break;
       }
       if(compare_board(current_move->nextMoves->board, gameboard)==1){
	 purge_branches(current_move->nextMoves);
	 current_move->nextMoves=NULL;
	 tmp_move=current_move->next;
	 free(current_move);
	 current_move=tmp_move;
       }
       else{
	 new_tree=0;
	 branchHead = current_move->nextMoves;
       }
     }
     }*/
   /*
   while(current_move){
     purge_branches(current_move->nextMoves);
     tmp_move=current_move;
     current_move=current_move->next;
     free(tmp_move);
   }
   if(!branchHead->board)
     branchHead->board = malloc(sizeof(struct board));
   memcpy(branchHead->board, gameboard, sizeof(struct board));
   if(new_tree==1)
       branchHead->list=scan(gameboard, computer_colour);
   if(!branchHead->list)
     return NULL;
   current_move=branchHead->list;
   move_head=current_move;
   sleep(1);*/
   move_head=scan(gameboard,computer_colour);
   current_move=move_head;
   if(!current_move)
     return NULL;
   while(current_move){
     current_move->nextMoves=malloc(sizeof(struct branchHead));
     current_move->nextMoves->board = updateBoard(gameboard, current_move, computer_colour);
     tmp_score=mini_max(current_move->nextMoves, 4, -INT_MAX, INT_MAX, computer_colour);
     if(tmp_score>best_score){
       move_index=count;
       best_score=tmp_score;
     }
     current_move=current_move->next;
     count++;
   }
   current_move=move_head;
   count=0;
   while(current_move){
     if(count==move_index)
       return current_move;
     current_move=current_move->next;
     count++;
   }
   return NULL;
 }
int compare_board(struct board *a, struct board *b ){
  int i,j;
  for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      if(a->squares[i][j]!=b->squares[i][j])
	return 1;
    }
  }
  if(a->canCastle[0]!=b->canCastle[0])
    return 1;
  if(a->canCastle[1]!=b->canCastle[1])
    return 1;
  if(a->enPassant!= b->enPassant)
    return 1;
  else
    return 0;
}

void initialise_board(struct board *board){
  int squares[8][8]={{4,1,0,0,0,0,7,10},
		     {2,1,0,0,0,0,7,8 },
		     {3,1,0,0,0,0,7,9 },
		     {5,1,0,0,0,0,7,11},
		     {6,1,0,0,0,0,7,12},
		     {3,1,0,0,0,0,7,9 },
		     {2,1,0,0,0,0,7,8 },
		     {4,1,0,0,0,0,7,12}};
 
  printf("Initialising board\n");
  memcpy(board->squares, squares, 64*sizeof(int));
  board->canCastle[0]=3;
  board->canCastle[1]=3;
  board->enPassant=-1;
}
